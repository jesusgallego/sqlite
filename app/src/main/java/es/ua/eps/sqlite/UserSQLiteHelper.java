package es.ua.eps.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mastermoviles on 12/12/16.
 */
public class UserSQLiteHelper extends SQLiteOpenHelper {

    private static final String TABLE = "Users";

    String sqlCreate = "CREATE TABLE " + TABLE + " (code INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, username TEXT, password TEXT)";
    String sqlAuthenticate = "SELECT * FROM " + TABLE + " WHERE username=? AND password=?";
    String sqlSelectAll = "SELECT * FROM " + TABLE;

    public UserSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
        ContentValues values = new ContentValues();
        values.put("name", "Administrador");
        values.put("username", "admin");
        values.put("password", "admin");

        db.insert(TABLE, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void createUser(String name, String username, String password) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("username", username);
        values.put("password", password);

        db.insert(TABLE, null, values);

        db.close();
    }

    public void deleteUser(String code) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE, "code=?", new String[]{code});
        db.close();
    }

    public void updateUser(String code, String name, String username, String password) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("username", username);
        values.put("password", password);

        db.update(TABLE, values, "code=?", new String[]{code});

        db.close();
    }

    public User authenticateUser(String username, String password) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery(sqlAuthenticate, new String[]{username, password});

        if (c.moveToFirst()) {
            User u = userFromCursor(c);
            u.setAuthenticated(true);

            return u;
        }
        db.close();
        return new User();
    }

    public List<User> getUsers() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery(sqlSelectAll, null);
        List<User> users = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                users.add(userFromCursor(c));
            } while (c.moveToNext());
        }

        db.close();
        return users;
    }

    private User userFromCursor(Cursor c) {
        String code = c.getString(c.getColumnIndex("code"));
        String name = c.getString(c.getColumnIndex("name"));
        String nick = c.getString(c.getColumnIndex("username"));
        String passwd = c.getString(c.getColumnIndex("password"));

        return new User(code, name, nick, passwd);
    }
}
