package es.ua.eps.sqlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mastermoviles on 15/12/16.
 */
public class UserAdapter extends ArrayAdapter<User> {
    public UserAdapter(Context context,int resource, List<User> objs) {
        super(context, resource, objs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        User u = getItem(position);

        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);

        textView.setText(String.format("%s (%s)", u.name, u.username));

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        User u = getItem(position);

        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);

        textView.setText(String.format("%s (%s)", u.name, u.username));

        return convertView;
    }
}
