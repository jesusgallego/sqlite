package es.ua.eps.sqlite;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by mastermoviles on 12/12/16.
 */
public class User implements Serializable {
    String code;
    String name;
    String username;
    String password;
    private boolean authenticated;

    public User() {
        setAuthenticated(false);
    }

    public User(String code, String name, String username, String password) {
        this.code = code;
        this.name = name;
        this.username = username;
        this.password = password;
    }


    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
