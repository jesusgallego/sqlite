package es.ua.eps.sqlite;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

public class UsersActivity extends AppCompatActivity {

    List<User> users;

    Spinner listUsersSpinner;
    UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listUsersSpinner = (Spinner) findViewById(R.id.spinner);

        getUsers();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newUser();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUsers();
    }

    private void newUser() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    public void onUpdate(View v) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(UserActivity.EXTRA_USER, (User) listUsersSpinner.getSelectedItem());
        startActivity(intent);
    }

    public void onDelete(View v) {
        UserSQLiteHelper helper = new UserSQLiteHelper(this, "DBUsers", null, 1);
        helper.deleteUser(((User)listUsersSpinner.getSelectedItem()).code);

        adapter = new UserAdapter(this, android.R.layout.simple_spinner_dropdown_item, users);
        listUsersSpinner.setAdapter(adapter);

        Toast.makeText(this, getText(R.string.deleted_successfully), Toast.LENGTH_SHORT).show();
    }

    private void getUsers() {
        UserSQLiteHelper helper = new UserSQLiteHelper(this, "DBUsers", null, 1);
        users = helper.getUsers();

        adapter = new UserAdapter(this, android.R.layout.simple_spinner_dropdown_item, users);
        listUsersSpinner.setAdapter(adapter);
    }

    public void onBack(View v) {
        finish();
    }

}
