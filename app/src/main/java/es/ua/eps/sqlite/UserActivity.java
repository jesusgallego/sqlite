package es.ua.eps.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserActivity extends AppCompatActivity {

    public static final String EXTRA_USER = "EXTRA_USER";

    User u;

    EditText name;
    EditText username;
    EditText password;
    Button actionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        name = (EditText) findViewById(R.id.user_name);
        username = (EditText) findViewById(R.id.user_username);
        password = (EditText) findViewById(R.id.user_password);
        actionButton = (Button) findViewById(R.id.user_action_button);

        u = (User) getIntent().getSerializableExtra(EXTRA_USER);
        if (u != null) {
            name.setText(u.name);
            username.setText(u.username);
            password.setText(u.password);

            actionButton.setText(getText(R.string.update));
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateUser();
                }
            });
        } else {
            actionButton.setText(getText(R.string.create));
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createUser();
                }
            });
        }
    }

    public void onBack(View v) {
        finish();
    }

    private void createUser() {
        UserSQLiteHelper helper = new UserSQLiteHelper(this, "DBUsers", null, 1);
        helper.createUser(name.getText().toString(), username.getText().toString(), password.getText().toString());
        Toast.makeText(this, R.string.create_successfully, Toast.LENGTH_SHORT).show();
    }

    private void updateUser() {
        UserSQLiteHelper helper = new UserSQLiteHelper(this, "DBUsers", null, 1);
        helper.updateUser(u.code, name.getText().toString(), username.getText().toString(), password.getText().toString());
        Toast.makeText(this, R.string.update_successfully, Toast.LENGTH_SHORT).show();
    }
}
