package es.ua.eps.sqlite;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    EditText username;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.login_name);
        password = (EditText) findViewById(R.id.login_password);
    }

    public void onLogin(View v) {
        UserSQLiteHelper helper = new UserSQLiteHelper(this, "DBUsers", null, 1);
        if (username.getText().length() > 0 && password.getText().length() > 0) {
            User u = helper.authenticateUser(username.getText().toString(), password.getText().toString());

            if (u.isAuthenticated()) {
                Intent intent = new Intent(this, WelcomeActivity.class);
                intent.putExtra(WelcomeActivity.EXTRA_USER, u);
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.invalid_pass, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.user_pass, Toast.LENGTH_SHORT).show();
        }
    }

    public void onExit(View v) {
        finish();
    }
}
