package es.ua.eps.sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {

    public static final String EXTRA_USER = "EXTRA_USER";

    User user;

    TextView name;
    TextView username;
    TextView email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        user = (User) getIntent().getSerializableExtra(EXTRA_USER);

        name = (TextView) findViewById(R.id.name);
        username = (TextView) findViewById(R.id.welcome_username);
        email = (TextView) findViewById(R.id.welcome_email);

        name.setText(user.name);
        username.setText(user.username);
        email.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.action_create_backup:
                return true;
            case R.id.action_restore_backup:
                return true;
            case R.id.action_users:
                intent = new Intent(this, UsersActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_config:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBack(View v) {
        finish();
    }
}
